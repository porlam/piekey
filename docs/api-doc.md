# API Documentation

You can find interactive API documentation at any Calckey instance. https://calckey.social/api-doc

You can also find auto-generated documentation for calckey-js [here](../packages/calckey-js/markdown/calckey-js.md).
