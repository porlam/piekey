<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [calckey-js](./calckey-js.md) &gt; [entities](./calckey-js.entities.md) &gt; [ServerInfo](./calckey-js.entities.serverinfo.md)

## entities.ServerInfo type

**Signature:**

```typescript
export declare type ServerInfo = {
	machine: string;
	cpu: {
		model: string;
		cores: number;
	};
	mem: {
		total: number;
	};
	fs: {
		total: number;
		used: number;
	};
};
```
